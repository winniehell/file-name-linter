#!/usr/bin/env node

const path = require('path')

const DirectoryWalker = require('./directory-walker')
const Linter = require('./linter')

if (process.argv.length !== 3) {
  console.error(`Usage: ${require('../package.json').name} config.json`)
  process.exit(1)
}

const configFileName = process.argv[2]
const config = require(path.resolve(configFileName))

const linters = {}

const callbacks = {
  lintingError: (rootDirectory, fileNamePattern, fileName) => console.error(` ✖  ${fileName} does not match ${fileNamePattern}!`),
  traversalError: (rootDirectory, err) => console.error(rootDirectory, err.message),
  traversalFinished: (rootDirectory) => console.log(` 🏁  Finished linting ${rootDirectory}`),
  ignorePath: (path) => console.log(` 👀  Ignoring ${path}`),
  traverseFile: (rootDirectory, filePath) => linters[rootDirectory].forEach((linter) => {
    if (!linter.check(filePath)) {
      process.exitCode = 1
    }
  })
}

if (config.callbacks) {
  Object.keys(callbacks).forEach((callbackName) => {
    const callback = config.callbacks[callbackName]
    if (callback) {
      callbacks[callbackName] = callback.bind(null, callbacks[callbackName])
    }
  })
}

const directoryWalker = new DirectoryWalker({
  errorCallback: callbacks.traversalError,
  fileCallback: callbacks.traverseFile,
  finishCallback: callbacks.traversalFinished,
  ignoreCallback: callbacks.ignorePath
})

config.rules = config.rules || {}
config.ignore = config.ignore || []
const directories = Object.keys(config.rules)
const ignoredDirectories = config.ignore

if (directories.length === 0) {
  console.error('The config seems to be empty!')
}

directories.forEach((rootDirectory) => {
  let allowedPatterns = config.rules[rootDirectory]

  if (!Array.isArray(allowedPatterns)) {
    allowedPatterns = [allowedPatterns]
  }

  linters[rootDirectory] = allowedPatterns.map((pattern) => new Linter(pattern, callbacks.lintingError.bind(null, rootDirectory)))
  directoryWalker.walk(rootDirectory, ignoredDirectories)
})
