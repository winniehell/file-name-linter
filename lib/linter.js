class Linter {
  constructor (fileNamePattern, errorCallback) {
    this.fileNamePattern = new RegExp(fileNamePattern)
    this.errorCallback = errorCallback || console.error
  }

  check (fileName) {
    if (this.fileNamePattern.test(fileName)) {
      return true
    }

    this.errorCallback(this.fileNamePattern, fileName)
    return false
  }
}

module.exports = Linter
