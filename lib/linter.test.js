const Linter = require('..').Linter

describe('Linter', () => {
  const testFileNamePattern = '\\d+'
  let linter

  beforeEach(() => {
    linter = new Linter(testFileNamePattern)
  })

  it('must pass check for valid names', (done) => {
    linter.errorCallback = (fileNamePattern, fileName) => done(new Error(`Expected ${fileName} to match ${fileNamePattern}!`))
    expect(linter.check('1234')).toBe(true)
    done()
  })

  it('must fail check for invalid names', (done) => {
    const invalidFileName = 'abc'
    linter.errorCallback = (fileNamePattern, fileName) => {
      expect(fileNamePattern.toString()).toBe(new RegExp(testFileNamePattern).toString())
      expect(fileName).toBe(invalidFileName)
      done()
    }
    expect(linter.check(invalidFileName)).toBe(false)
  })
})
