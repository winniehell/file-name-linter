const path = require('path')

const config = {
  callbacks: {},
  rules: {},
  ignore: [path.join(__dirname, 'ignored')]
}

const allowedFileExtensions = ['js', 'json']
config.rules[__dirname] = `^[a-z-/]+\\.${allowedFileExtensions.join('|')}$`

config.callbacks.traverseFile = (defaultCallback, rootDirectory, filePath) => defaultCallback(rootDirectory, path.relative(rootDirectory, filePath))
config.callbacks.ignoreCallback = (filePath) => console.error('ignoreCallback:', filePath)

module.exports = config
